<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_product', function (Blueprint $table) {
            $table->unsignedBigInteger("cart_id");
            $table->foreign("cart_id")->references("id")->on("carts");
            $table->unsignedBigInteger("product_id");
            $table->foreign("product_id")->references("id")->on("products");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cart_product",function (Blueprint $table){
            $table->dropForeign(["cart_id"]);
            $table->dropForeign(["product_id"]);
        });
        Schema::dropIfExists('cart_product');
    }
}
