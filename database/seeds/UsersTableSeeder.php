<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = \App\Models\User::create([
            "name"=>"Admin Admin",
            "email"=>"admin@mycommerce.com",
            "password"=>bcrypt("superpassword@")
        ]);

        $adminUser->assignrole('admin');
    }
}
