### Ecommerce -API

---

This is the repository for a basic Ecommerce-api mockup

- [Requisites](#requisites)
- [Files](#files)
- [Code Structure](#code-structure)
- [Used Libraries](#used-libraries)
- [Installation](#installation-instructions)
- [Windows Install](#windows)
- [Linux/Mac](#linux--mac)
- [To-Do](#to-do)
- [Additional Stuff](#additional-stuff)


##### Requisites

* PHP 7.3
* Mysql 5.7
* Docker (Optional)



This project is based on Laravel 6, for information on how to install the framework, please refer to the official documentation, or use one of the recommended programs

Windows - Laragon

Linux / Mac - Docker ( [Based on my blog post](https://qiita.com/dabiddo/items/6c8b0d496439fecda7dc) )

##### Files
* [Postman Json Collection](/resources/docs/1.0/ecommerce-api.postman_collection.json) (v2.1 Postman needed)
* [Insomnia Json collection](/resources/docs/1.0/ecommerce_api.json)
* [Database ER Diagram MWB file](/resources/docs/1.0/ecommerce-api_diagram.mwb)
* [Database ER DiagramPNG file](/resources/docs/1.0/ecommerce-api_diagram.png)

##### Code structure

For the most part, I followed stardard Laravel folder structure based on PSR-4,
The **Models** are inside their own folder.
##### \App\Http\Requests
Here are the request validators, most of them, only check that the required parameters are on the request, some also check that the request user is authenticated, and a few also check if the user has Admin role.
##### \App\Observers
I added some **Oberservers** for the User, whenever a new user is registered, it automatically assigns a role.

The CartProduct observer, re-calculates the **total_payment** amount based on the price of every product inside the cart.

***Side Note:***
There is **No** seeder for products, you will have to register some, once you get the project up and running.

##### \database\migrations
These are the migrations for creating tables on the database
##### \database\seeds
These seeds are registries created to initialize the database, it creates the basic roles [Admin, User], and also creates the Admin user account.
##### \resources\
In this folder, lives the files for LaRecipe, showing the **Api Documentation**, in order to see it, you must first get the project up and running,
once done, you can visit the site
```html
htt://<my server>/docs
``` 


### Used libraries

- [Laravel/Passport](https://laravel.com/docs/6.x/passport) : for generating JWT token  ( [Based on my blog post](https://qiita.com/dabiddo/items/7e4a9122fe871788065c) )
- [cloudcake/laravel-shovel](https://github.com/cloudcake/laravel-shovel) : for generating standard API responses.(seems the author is updating the library, the documentation is missing)
- [spatie/laravel-permission](https://github.com/spatie/laravel-permission) : For administering users roles to allow certain actions on the endpoints
- [LaRecipe](https://larecipe.binarytorch.com.my) : For the API documentation [localhost/docs](http://localhost/docs)

## Installation Instructions
While I try to make this project as easy to understand, and easy to install, you will need to hav experience installing laravel,
if not, here is a little instructions on how to get this project up and running.

#### Windows
For windows, I use [Laragon](https://laragon.org), is easy to install, and comes with support for laravel out of the box.
If you are going to this route, these are the instructions.

* Install Laragon

Clone the repository inside ***C:\laragon\wwww***
```php
git clone https://dabiddo@bitbucket.org/dabiddo/ecommerce-api.git
```

This will create a new folder called **ecommerce-api** with the project files.
Since laragon comes with PHP pre-installed, you can use your favorite cmd program to run
the artisan commands inside the project folder
```php
composer install
```
Once instaled, make a copy of the **.env.example** and rename it as **.env**
Using laragons built in Mysql, create an empty database, and set the mysql credentials inside the
newly created **.env** file to your credentials
```mysql
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ecommerce
DB_USERNAME=root
DB_PASSWORD=
```
Once that is done, you can continue with the rest of the commands to get started.
```php
php artisan migrate
php artisan passport:install
php artisan db:seed
```
Usually, laragon sets a hostname on windows machines, if this is the case, you can reload laragon, and visit the
project local domain
```html
http://ecommerce-api.dev
```
If you see the standard laravel home page, that means you are up and running.

#### Linux / Mac
For linux/mac the recommended setup is using docker, the repository comes ready to be used with **docker-compose**

Clone the repository inside your favorite directory, recommended is inside **/home/<user>**

Once cloned,enter the project directory and run a temporary compose container
```dockerfile
docker run --rm -v $(pwd):/app composer install
```

Remember to give write permission to the **storage** folder inside the project
```bash
sudo chmod -R 777 storage/
```

Once cloned and installed, you can copy the **env.example** and rename it as **.env**
```bash
cp .env.example .env
```
Thats it, the new .env file is ready to be used by docker.
```dockerfile
docker-compose up --build
```

Once the containers are running, you can execute the **php artisan** commands, by referencing the api-server container
```bash
docker-compose exec api-server php artisan migrate
docker-compose exec api-server php artisan passport:install
docker-compose exec api-server php artisan db:seed
```

Docker uses the **localhost** domain, so you should be able to see the standard home page.

**Stop Containers**
To stop the project, you can do a normal composer-down
```bash
docker-compse down
```
This will stop, the containers and remove al temporary files
##### TO-DO
As with every project, because of the lack of time, these are some pending to-dos

* Add validation of cart update to check if user is owner of the cart

* Modify the cart_product pivot table to allow deletion of product by id, and add quantity field, as to not repeat product objects

* fix the auto decrement of available stock when cart is set to paid

* If given more time, and more details on the overall project, I would've liked to add unit tests, I didn't want to spend too
much time generating these success cases.

* While doing the project, I only tested using happy cases, so doing more wrong test to add more validations would've been nice.

### Additional Stuff
**larecipe**

I had some minor issues switching between my linux and windows machine to show the Api Docs, on windows I had to clear the whole cache
```php
php artisan optimize:clear
php artisan route:clear
php artisan config:clear
php artisan cache:clear
```
##### \App\Exceptions\Handler.php
I modified the ***render*** function, to add support for a few exceptions, and to return a standard JSON response using the **laravel-shovel** library.

##### Personal Comments
For the most part, I used **artisan commands** to create the structure, that is why you'll see a lot of empty functions inside the controllers.

For the User Roles, I could've used the **laravel policies** that come with the framework, but I really like using the **spatiebe** library.
