# Products

---

- [Introduction](#intro)
- [Register Product](#store)
- [All Products](#index)
- [Single Product](#show)
- [Update Product](#update)
- [Delete Product](#delete)

<a name="intro"></a>
## Introduction
The following endpoints are the main center of the system, the Products are what objects are being sold in the store.
<a name="store"></a>
### Register a Product [Admin]
By default, the system doesn´t have any products in the database, so you will need to register at least one product
In order to use this endpoint, you will need to log in as Admin, and retrieve a Admin JWT.
```json
POST: http://localhost/api/products
Header Authorization: Bearer xxxxxxxxxxxx
Json Body:
{
	"title":"Headphones zz",
	"description":"new and improve",
	"price":25,
	"available_stock":10
}
```
If successfull, the endpoint will answer with a **200** reponse withe the same json object, adding the ID
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "title": "Headphones zz",
    "description": "new and improve",
    "price": 25,
    "available_stock": 10,
    "updated_at": "2020-01-28 02:57:50",
    "created_at": "2020-01-28 02:57:50",
    "id": 2
  }
}
```
<a name="index"></a>
### All Products
This endpoint shows all products in the database.
```json
GET http://localhost/api/products
```
When successfull, the endpoint will answer with a **200** response showing all the products in the system
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": [
    {
      "id": 2,
      "title": "Headphones xx",
      "description": "new and improve",
      "price": 25,
      "available_stock": 10,
      "status": "available",
      "created_at": "2020-01-28 02:14:23",
      "updated_at": "2020-01-28 02:14:23"
    },
    {
      "id": 3,
      "title": "Headphones ww",
      "description": "new and improve",
      "price": 25,
      "available_stock": 10,
      "status": "available",
      "created_at": "2020-01-28 02:14:35",
      "updated_at": "2020-01-28 02:14:35"
    }
  ]
}
```
<a name="show"></a>
### Single Product
This endpoint shows the information regarding a single product, for it you need to know the **ID** of the product
```json
GET http://localhost/api/products/{id}
```
When successfull, the endpoint will return a **200** reponse with the information of the desired product based on its **ID**
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "id": 1,
    "title": "Headphones",
    "description": "new and improve",
    "price": 20,
    "available_stock": 10,
    "status": "available",
    "created_at": "2020-01-28 02:02:53",
    "updated_at": "2020-01-28 02:02:53"
  }
}
```
<a name="update"></a>
### Update Product [Admin]
This endpoint is used to modify information regarding a product, the only modifiable fields are
[title,description,price,available_stock]
In order to use this endpoint, you need to login as Admin, and retrieve a valid Admin JWT Token
and retrieve an existing Product ID,
```json
PUT: http://localhost/api/products/{ID}
Header Authorization: Bearer XXXXXXXXXX
json body:
{
	"title":"Headphones zz",
	"description":"new and improve",
	"price":20,
	"available_stock":10
}
```
When successfull, the endpoint will answer with a **200** response with the updated product data
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "id": 1,
    "title": "Headphones zz",
    "description": "new and improve",
    "price": 20,
    "available_stock": 10,
    "status": "available",
    "created_at": "2020-01-28 02:02:53",
    "updated_at": "2020-01-28 02:13:47"
  }
}
```

<a name="delete"></a>
### Delete Product [Admin]
This endpoint is used to delete a product,
In order to use this endpoint, you need to login as Admin, and retrieve an Admin JWT token,
also, you need to know the ID of the product to be deleted
```json
DELETE: http://localhost/api/products/{ID}
Header Authorization: Bearer xxxxxxxxxxx
```
When successfull, the endpoint will return a **200** reponse, no data
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": null
}
```
