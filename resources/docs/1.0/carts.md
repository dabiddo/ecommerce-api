# Carts

---

- [Introduction](#intro)
- [Add Products to Cart](#add)
- [Cart Checkout](#checkout)

<a name="intro"></a>
## Introduction
Carts are objects where you store the products before paying.
Every user after login for the 1st time, will have a **cart** created with no products
Please reference [Users](/{{route}}/{{version}}/users) in order to locate your **Cart ID**

<a name="add"></a>
## Add Products to Cart
Once you have logged in either as Admin or as a normal user, you will have a valid JWT token, and an empty cart,
you can always reference the [Products](/{{route}}/{{version}}/products) page to get some products id and begin adding to your cart
```json
POST: http://localhost/api/cart/{CART_ID}/product
Header Authorization: Bearer xxxxxxxxxxx
json body:
{
	"product_id":2
}
```
Upon success, the endpoint will return a **200** response with the cart, and the array of products currently inside the cart
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "id": 2,
    "user_id": 2,
    "status": "pending",
    "total_payment": 75,
    "created_at": "2020-01-28 02:51:05",
    "updated_at": "2020-01-28 02:58:58",
    "products": [
      {
        "id": 1,
        "title": "Headphones ww",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:51:21",
        "updated_at": "2020-01-28 02:51:21"
      },
      {
        "id": 2,
        "title": "Headphones zz",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:57:50",
        "updated_at": "2020-01-28 02:57:50"
      },
      {
        "id": 2,
        "title": "Headphones zz",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:57:50",
        "updated_at": "2020-01-28 02:57:50"
      }
    ]
  }
}
```
Notice the cart status is **pending** this is a general status that allows to add more products to the same cart.
this status changes once you do a checkout
The Cart **total_payment** gets calculated automatically when adding new products to the cart.

<a name="checkout"></a>
## Cart Checkout
As the name implies, this endpoint is used for the purpose of paying for the cart, taking into account the **total_payment**

For the purpose of simplicity, I created this endpoint, to set the status of the given cart id as **paid**

In order to use this endpoint, you will need your User JWT Token, and your Cart ID
```json
POST: http://localhost/api/cart/{CART_ID}/checkout
Header Authorization: Bearer xxxxxx
```
After success, the endpoint will return a **200** response, with the Cart object and its status set to **paid**
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "id": 2,
    "user_id": 2,
    "status": "paid",
    "total_payment": 75,
    "created_at": "2020-01-28 02:51:05",
    "updated_at": "2020-01-28 03:12:03",
    "products": [
      {
        "id": 1,
        "title": "Headphones ww",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:51:21",
        "updated_at": "2020-01-28 02:51:21"
      },
      {
        "id": 2,
        "title": "Headphones zz",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:57:50",
        "updated_at": "2020-01-28 02:57:50"
      },
      {
        "id": 2,
        "title": "Headphones zz",
        "description": "new and improve",
        "price": 25,
        "available_stock": 10,
        "status": "available",
        "created_at": "2020-01-28 02:57:50",
        "updated_at": "2020-01-28 02:57:50"
      }
    ]
  }
}
```
