# Users

---

- [Introduction](#intro)
- [Admin](#admin)
- [Register](#register)
- [Login](#login)

<a name="intro"></a>
## Intro
The system comes with a built in **admin** account, which is installed when you run the db:migrations
in the installation.

<a name="admin"></a>
### Admin
This built in account, is used for the most part, to create / edit / delete products.
while the endpoints to view products are open to anyone.

##### credentials
```html
email: admin@myecommerce.com
password: superpassword@
```
<a name="register"></a>
### Register
The register endpoint is used to create a new user
```html
POST http://localhost/api/register

json body:
{
	"name":"David Torres",
	"last_name":"Torres",
	"email":"dabiddo@gmail.com",
	"password":"cocacola1",
	"password_confirmation":"cocacola1"
}
```

If successfull, the endpoint will answer with a **200** response
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZDQ2MjE1NzY0NjcxNDNkNDZkZWRmM2U5OWY5OGVkZTQ0N2U1NjI3ZDExMDkwYTA0MjVlYWJmYzU1ZjY1ZjYwNGRmNjhhMTZjMWVjNzE0ZDAiLCJpYXQiOjE1ODAxNzk4NTUsIm5iZiI6MTU4MDE3OTg1NSwiZXhwIjoxNjExODAyMjU1LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.uJTJ1DAKBqp1hCTS5__UNWRa_BcFBjA3Y0vtDwzPOwhA6ExkdJmr2ECcKA51wO8reASutEtrmknZ-Ou4n83SzXEuGvpEQRxfevGPtPP3mMtkhrlb4VXMjVTAtgzp2uf6hXfjWH6cjHh04GVdJhHqBvtlgUAX8wVUTGrYcVPfUN74NxuGQ_i_LiHnRvH_tyul4ZpP2aFU0H4bBGzkmt84nYUUVnb04ptnOUyxZqQw7a71mFrom58jsA65Yb3Ntsbkj7ePBaazsv-vqIPXNj-C9g3AvkLJ8Ua5Lszj5IxJXX-XIGfS692VER0xLIZOX88ER-k7hd7NgdHov_SypO50AIunCFWJfnHTVhwQMf7oSAmZZEcSB83_5XYVGYld2s-FA1TIJe7I5iPxw16PKVl3cS3cghUcjhWSu9Q9DOj6sfxLZ_iAMlnM9zW5s2NTY76UhJnBskSVyDQVv111168pRvKjonm_OMzwDZKxRhnTsT7HmROUq1c0s_0NFQCEBTTkKZIIjjWgmnQqA6WgbZlgEQWr4qupMquZDQGwYUd9GhxmI31vbwyVhU9Wrz37JJqCxeyMtqQ7VOzJ1fDsfw8KneP7Vgz2VouHk519ag3PDCdPxKYyhfRnA0s7kTGwuloJ3vS7h-guD4Q6GSYYoGYBbo5RfNV36xngw9S96XSC7cQ",
    "name": "David Torres"
  }
}
```
<a name="login"></a>
### Login
The login endpoint is used to generate a valid JWT token for already registered users.
```json
POST: http://localhost/api/login

json body:
{
	"email":"dabiddo@gmail.com",
	"password":"cocacola1"
}
```

If successfull, the endpoint will answer with a 200 response
```json
{
  "meta": {
    "code": 200,
    "status": "success",
    "message": "OK"
  },
  "data": {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNTQ2MjU3YjFmNzhjZTQzMDRlZWM0MWFhNTQ3OTdhZWRhOWYwMjNlYzk5OTdhYmNiNjkzZWM3NTBhOTNmODA0NzZlYTE2NGJmOWFlN2MxNjQiLCJpYXQiOjE1ODAxODI4NjAsIm5iZiI6MTU4MDE4Mjg2MCwiZXhwIjoxNjExODA1MjYwLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.WmPrL23p3KYR6wjmUAVmOrQ2MUz7Ksns21BjSz6gn-NN8Gh0IpKS_szA-RS747pKwX2NsQcnNr-48tOHuVnPgu5N4SDaM2g0VS-afaOLuzZcsM4X_et9ciAcC77xSo4drhPdNqwqqoUtk7GtCaXAJqU09pMq7B82zIyYlhYi3j-vuic8-zEwFpZLUnx_LVA60Yl85BETIghMIEXEfTrlyE15ekZ7_AmVzaEQmzHLTF-1Uje8Pgz-Y3J5I5mCVFn9xdED4oyoHUR0Iy53pTHycE1jsx8F-016zRqrPXxFaoWrra7kNPVfqMap0PE6TzBOd0iaH5R0xWdLJMHC98H0_bQk133zJwF2BSEMK16CWCsx0AvRHyaLERJrHTU2dPZewKDL35Xn1WqG_jkiwAJXV2XkUeP1d6WRdJw-3ILOoYAdeN0hozW_T31hK-LrTmew7zNvtfdvKyovdBZiXL3cu2KIgYnTlePnMvfEQdnQHo8_v2VDh0ynQbRIi18hXOn0Qr8FXEl9Tl7lPaXRgnfS4rXpdUQeY_J0BkraD8oDo3dtTgjuT400T7QRaTWUWTADRkr_LDfyQM7amjc8dEWujt2DznT9VpGBPzyEcKk41SQ1nUj-8i8duk3xDOddlleiRUwJ5BQNhax3VK075uLNmy_AQu_YKtp17PjgnjMGbk0",
    "name": "David Torres",
    "cart": {
      "user_id": 2,
      "updated_at": "2020-01-28 03:41:00",
      "created_at": "2020-01-28 03:41:00",
      "id": 3,
      "products": []
    }
  }
}
```
The notecibale difference between **loging** and **register**, is that in the login endpoint
the user is able to see their **cart**, this is created automatically for every user on creation, in order to start adding products to it.
