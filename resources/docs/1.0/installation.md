# Installation

---

- [Installation](#installation)
- [Windows](#windows)
- [Linux/Mac](#unix)

<a name="installation"></a>
## Installation Instruction
While I try to make this project as easy to understand, and easy to install, you will need to hav experience installing laravel,
if not, here is a little instructions on how to get this project up and running.
<a name="windows"></a>
#### Windows
For windows, I user [Laragon](https://laragon.org), is easy to install, and comes with support for laravel out of the box.
If you are going to this route, these are the instructions.
* Install Laragon

Clone the repository inside **c:\Laragon\www\**
```php
git clone https://dabiddo@bitbucket.org/dabiddo/ecommerce-api.git
```

This will create a new folder called **ecommerce-api** with the project files.
Since laragon comes with PHP pre-installed, you can use your favorite cmd program to run
the artisan commands inside the project folder
```php
composer install
```
Once instaled, make a copy of the **.env.example** and rename it as **.env**
Using laragons built in Mysql, create an empty database, and set the mysql credentials inside the
newly created **.env** file to your credentials
```mysql
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ecommerce
DB_USERNAME=root
DB_PASSWORD=
```
Once that is done, you can continue with the rest of the commands to get started.
```php
php artisan migrate
php artisan passport:install
php artisan db:seed
```
Usually, laragon sets a hostname on windows machines, if this is the case, you can reload laragon, and visit the
project local domain
```html
http://ecommerce-api.dev
```
If you see the standard laravel home page, that means you are up and running.

<a name="unix"></a>
#### Linux / Mac
For linux/mac the recommended setup is using docker, the repository comes ready to be used with **docker-compose**

Clone the repository inside your favorite directory, recommended is inside **/home/<user>**

Once cloned,enter the project directory and run a temporary compose container
```dockerfile
docker run --rm -v $(pwd):/app composer install
```

Once cloned and installed, you can copy the **env.example** and rename it as **.env**
Thats it, the new .env file is ready to be used by docker.
```dockerfile
docker-compose up --build
```

Once the containers are running, you can execute the **php artisan** commands, by referencing the api-server container
```bash
docker-compose exec api-server php artisan migrate
docker-compose exec api-server php artisan passport:install
docker-compose exec api-server php artisan db:seed
```

Docker uses the **localhost** domain, so you should be able to see the standard home page.

##### Next Step
[Register, Login & Admin](/{{route}}/{{version}}/users)
