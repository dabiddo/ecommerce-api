# Exceptions

---

- [Exceptions](#intro)

<a name="intro"></a>
## Exceptions
The system while not complex, does have a few excpetion handlers

|       Title       | Code |                         Description                          |
| :---------------: | ---- | :----------------------------------------------------------: |
|      Success      | 200  |             The endpoint request was successfull             |
|   UnAuthorized    | 401  | * The request does not have authorization header<br />* The requested user does not have the assigned role to make the request. |
| Validation Errors | 422  | The request did not pass validation rules needed, or has missing parameters. |
|   General Error   | 500  |                All other unhandle exceptions                 |

