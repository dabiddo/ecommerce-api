# Overview

---

- [Introduction](#intro)

<a name="intro"></a>
## Official API Documentation

Welcome, you have reached the API documentation, over the next few pages, I´ll try to explain
how the API works, and to give a general idea on the stardard flow of information.

### Files
In order to speed up the testing process, here are some files you may need to test the api on your local PC
* [Postman Json Collection](https://bitbucket.org/dabiddo/ecommerce-api/src/master/resources/docs/1.0/ecommerce-api.postman_collection.json) (v2.1 of postman needed)
* [Insomnia Json Collection](https://bitbucket.org/dabiddo/ecommerce-api/src/master/resources/docs/1.0/ecommerce_api.json)
* [Database ER Diagram MWB file](https://bitbucket.org/dabiddo/ecommerce-api/src/master/resources/docs/1.0/ecommerce-api_diagram.mwb)
* [Diagram Image](https://bitbucket.org/dabiddo/ecommerce-api/src/master/resources/docs/1.0/ecommerce-api_diagram.png)

![Diagram](https://bitbucket.org/dabiddo/ecommerce-api/raw/dd308c7b95b3b97e7e27340920e8e3070dc1d13d/resources/docs/1.0/ecommerce-api_diagram.png)

##### Next Step
[Installation](/{{route}}/{{version}}/installation)
