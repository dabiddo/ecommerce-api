<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['ApiRequest',"ApiResponse"]],function (){
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::resource('products',ProductsController::class)->only('index','show');

    Route::middleware('auth:api')->group(function () {
        Route::resource('products',ProductsController::class)->only('store','update','destroy');
        Route::resource("cart/{cart}/product","CartProductController");
        Route::post("cart/{cart}/checkout","CartsController@checkout");
    });
});

