<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if(!$request->user()){
            return response("",401)
                ->withMeta("message","No User");
        }
        if($request->acceptsJson())
        {
            if($exception instanceof ValidationException)
            {
                return response($exception->validator->errors()->messages(),422)
                    ->withMeta("message","validation errors");
            }

            if($exception instanceof AuthorizationException){
                return response("",401)
                    ->withMeta("message",$exception->getMessage());
            }
            return response("",$exception->getCode())
                ->withMeta("message",$exception->getMessage());
        }
        return parent::render($request, $exception);
    }
}
