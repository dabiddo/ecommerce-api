<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRegisterRequest;
use App\Models\Cart;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(PostRegisterRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        return response($success);
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['name'] =  $user->name;

            //After successful login, get default cart or create cart
            $cart = Cart::where('user_id',$user->id)->where(["status"=>"pending"])->first();
            if(!$cart){
                $cart = Cart::create(['user_id'=>$user->id]);
                $cart->fresh(); //refresh model to view status, not visible when created for the 1st time
            }

            $cart->load(['products']);
            $success['cart']=$cart;

            return response($success);
        }
        else{
            throw new \Exception("Wrong Password or Email",401);
        }
    }
}
