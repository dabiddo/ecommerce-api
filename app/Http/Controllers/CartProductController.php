<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class CartProductController extends Controller
{
    public function store(Cart $cart, Request $request){

        CartProduct::create([
            "cart_id"=>$cart->id,
            "product_id"=>$request->input("product_id")
        ]);

        $cart->refresh();
        $cart->load("products");
        return response($cart);
    }
}
