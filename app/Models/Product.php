<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["title","description","price","available_stock","status"];

    protected $visible = ["id","title","description","price","available_stock","status","created_at","updated_at",'carts'];

    public function carts(){
        $this->belongsToMany("App\Models\Cart");
    }
}
