<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['user_id',"status"];

    protected $visible = ['id','user_id','created_at','updated_at','status','products','total_payment'];

    public function products(){
        return $this->belongsToMany("App\Models\Product");
    }
}
