<?php

namespace App\Observers;

use App\Models\Cart;
use App\Models\CartProduct;

class CartProductObserver
{
    /**
     * Whenever a user adds a product to the cart, re-calculate the total_amount to be paid,
     * based on the price of every product.
     *
     * @param  \App\CartProduct  $cartProduct
     * @return void
     */
    public function created(CartProduct $cartProduct)
    {
        $cart = Cart::find($cartProduct->cart_id)->load("products");

        foreach ($cart->products as $product){
            $cart->total_payment += $product->price;
        }

        $cart->save();
    }

    /**
     * Handle the cart product "updated" event.
     *
     * @param  \App\CartProduct  $cartProduct
     * @return void
     */
    public function updated(CartProduct $cartProduct)
    {
        //
    }

    /**
     * Handle the cart product "deleted" event.
     *
     * @param  \App\CartProduct  $cartProduct
     * @return void
     */
    public function deleted(CartProduct $cartProduct)
    {
        //
    }

    /**
     * Handle the cart product "restored" event.
     *
     * @param  \App\CartProduct  $cartProduct
     * @return void
     */
    public function restored(CartProduct $cartProduct)
    {
        //
    }

    /**
     * Handle the cart product "force deleted" event.
     *
     * @param  \App\CartProduct  $cartProduct
     * @return void
     */
    public function forceDeleted(CartProduct $cartProduct)
    {
        //
    }
}
